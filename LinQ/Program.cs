﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQ
{
    class Program
    {
        static void Main(string[] args)
        {
            //Queries sencillos con arreglos y reflexion

            //ejemplo con numeros
            //Creamos un arreglo sobre el cual trabajar
            int[] numeros = { 1, 5, 7, 3, 5, 9, 8 };

            //Hacemos el query
            IEnumerable<int> valores = from n in numeros
                                       where n > 3 && n < 8
                                       select n;
            //mostramos los resultados
            foreach (int num in valores)
                Console.WriteLine(num);
            Console.WriteLine("--------------------");
            Console.WriteLine("Presione una tecla para continuar...");
            Console.ReadKey(true);

            //Ejemplo con cadenas
            //Creamos un arreglo sobre el cual trabajar
            string[] postres = { "pay de manzana", "pastel de chocolate", "manzana caramelizada", "fresas con crema" };

            //Hacemos el query
            IEnumerable<string> encontrados = from p in postres
                                              where p.Contains("manzana")
                                              orderby p
                                              select p;
            //mostrar los resultados
            foreach (string postre in encontrados)
                Console.WriteLine(postre);

            Console.WriteLine("--------------------");
            Console.WriteLine("Presione una tecla para continuar...");
            Console.ReadKey(true);

            //Hacemos reflexion
            InformacionResultados(valores);
            Console.WriteLine("--------------------");
            InformacionResultados(encontrados);
            Console.WriteLine("Presione una tecla para continuar...");
            Console.ReadKey(true);
        }

        static void InformacionResultados(object pResultados)
        {
            Console.WriteLine("Tipo {0}", pResultados.GetType().Name);
            Console.WriteLine("Locacion {0}", pResultados.GetType().Assembly.GetName().Name);
        }
    }
}
