﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LINQXML17
{
    class Program
    {
        static void Main(string[] args)
        {
            //Obtenemos informacion XML a partir de una cadena

            string alumnos = @"<Alumnos>
<Alumno Nombre='Jose'>
<Curso> Programacion </Curso>
<Calificacion> 8 </Calificacion>
</Alumno>
<Alumno Nombre = 'Susana'>
<Curso> UML </Curso>
<Calificacion> 9 </Calificacion>
</Alumno>
<Alumno Nombre = 'Maria'>
<Curso> Orientado a objetos</Curso>
<Calificacion> 10 </Calificacion>
</Alumno>
<Alumno Nombre = 'Luis'>
<Curso> UML </Curso>
<Calificacion> 10 </Calificacion>
</Alumno>
</Alumnos>";
            
            //recomendaciones es eliminar todos los espacios vacios en las etiquetas
            XElement alumnosx = XElement.Parse(alumnos);    //Pasamos la cadena para que haga parse

            //Mostramos el xml
            Console.WriteLine(alumnosx);

            Console.Write("Presione una tecla para continuar...");
            Console.ReadKey();
        }
    }
}
