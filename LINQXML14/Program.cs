﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LINQXML14
{
    class Program
    {
        static void Main(string[] args)
        {
            //XML es muy utilizado, pero es tedioso trabajar con el, aunque sencillo
            //El framework provee System.Xml.dll y XmlReader, XmlWriter
            //Pero es mas sencillo trabacon con LINQ para XML

            //Cada parte de XML: declaración, elemento, atributos. Pueden representarse por clases
            //Si esas clases tienen colecciones, entonces podemos representar un arbol que
            //describe al documento XML, esa es la base de DOM (Document Object Model)

            //LINQ para XML tiene un DOM conocido como X-DOM y operadores extra
            //XObject       -Clase abstracta que es la base para todo el contenido de XML
            //XNode         -Clase base para la mayoria del contenido XML, excepto atributos
            //XContainer    -Define miembros para trabajar con sus hijos y es la clase padre de XElement y XDocument
            //XElement      -Define a un elemento
            //XDocument     -Representa la raiz de un arbol XML, en realidad envuelve a un XElement que actua como raiz
            //              y lo podemos usar para adicion la declaracion e instrucciones de procesamiento.
            //              Se puede usar X-DOM sintener un XDocument

            //Creamos un documento sencillo de XML con LINQ (Forma 1)
            XElement raiz = new XElement("raiz");
            XElement el1 = new XElement("Elemento1");
            el1.Add(new XAttribute("atributo", "valor"));
            el1.Add(new XElement("Anidado", "Contenido"));

            raiz.Add(el1);
            Console.WriteLine(raiz);
            Console.WriteLine("------------");

            //Esta forma de crear el documento se conoce como construccion funcional (Forma 2 mejor de acuerdo al expositor)
            XElement documento = new XElement("Alumnos",
                new XElement("Ana", new XAttribute("ID", "10100"),
                    new XElement("Curso","Administracion"),
                    new XElement("Promedio","10")
                    ),  //fin de Ana
                new XElement("Luis", new XAttribute("ID", "25350"),
                    new XElement("Curso", "Programacion"),
                    new XElement("Promedio", "9.5")
                    )  //fin de Luis
                
                );  //fin de alumnos

            //imprimimos el documento
            Console.WriteLine(documento);
            //Escribimos el documento a disco
            documento.Save("Alumnos.xml"); 
            Console.WriteLine("------------");




           

            Console.Write("Presione una tecla para continuar...");
            Console.ReadKey();

        }
    }
}
