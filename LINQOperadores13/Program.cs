﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQOperadores13
{
    class Program
    {
        static void Main(string[] args)
        {
            //Hay tres categorias para los operadores de query
            //secuencia a secuencia (secuencia de entrada, secuencia de salida)
            //Secuencia de entrada, elemento sencillo o escalar (valor numerico)
            //Nada de entrada, secuencia de salida

            //Secuencia a secuencia
            //Filtro: Where, Take, TakeWhile, Skip, SkipWhile, Distinct
            //Proyeccion: select, SelectMany
            //Union: Join, GroupJoin, Zip
            //Ordenamiento: OrderBy, ThenBy, Reverse
            //agrupamiento: GroupBy
            //Operadores de conjuntos: Concat, Union, Intersect, Except
            //Conversion import: OfType, Cast
            //Conversion export: ToArray, ToDictionary, ToLookup, AsEnumerable, AsQueryable

            //secuencia a elemento o escalar
            //Operadores de elemento: First, FirsOrDefault, Last, LastOrDefault, Single, SingleOrDefault, ElementAt, ElementAtOrDefault, DefaultEmpty
            //Agregación: Aggregate, Average, Count, LongCount, Sum, Max, Min
            //Cuantificador: All, Any, Contains, SequenceEqual

            //Nada de entrada, Secuencia de salida
            //Generacion: Empty, Range, Repeat

            //////////////////////////////////////////////////////////////////////////////////////////////////////

            //Filtro

            //Where regresa un  subconjunto de elementos que satisfacen una condicion
            //Take regresa el primer elemento n e ignora el resto
            //Skip ignora los primeros n elementos y regresa el resto
            //TakeWhile emite elementos de la secuencia de entrada hasta que el predicado es falso
            //SkipWhile Ingnora los elementos de la secuencia de entrada hasta que el predicado es falso, luego emite el resto
            //Distinct Regresa una secuencia que excluye a los duplicados

            //Where 
            //Aparte de lo que hemos visto, Where permite un segundo argumento de tipo int que simboliza el indice del elemento
            //Esto se conoce como filtrado por indice 

            //Creamos un arreglo sobre el cual trabajar
            string[] postres = { "pay de manzana", "pastel de chocolate", "manzana caramelizada", "fresas con crema", "pay de pera y manzana" };

            Console.WriteLine("--- Where ---\r\n");
            IEnumerable<string> r1 = postres.Where((n, i) => i % 2 == 0);
            //Mostramos los resultados
            foreach (string postre in r1)
                Console.WriteLine(postre);
            Console.WriteLine("---------------");

            IEnumerable<string> r2 = from p in postres
                                     where p.StartsWith("pay")
                                     select p;
            //Mostramos los resultados
            foreach (string postre in r2)
                Console.WriteLine(postre);
            Console.WriteLine("---------------");

            IEnumerable<string> r3 = from p in postres
                                     where p.EndsWith("manzana")
                                     select p;
            //Mostramos los resultados
            foreach (string postre in r3)
                Console.WriteLine(postre);
            Console.WriteLine("---------------");

            Console.WriteLine("--- TakeWhile ---\r\n");

            //TakeWhile enumera la secuencia de entrada y emite cada elemento hasta que el predicado es falso
            //ignora el resto

            //Creamos un arreglo sobre el cual trabajar
            int[] numeros = { 1, 5, 7, 3, 5, 9, 8, 11, 2, 4 };
            var r4 = numeros.TakeWhile(n => n < 8);
            //Mostramos los resultados
            foreach (int n in r4)
                Console.WriteLine(n);
            Console.WriteLine("---------------");

            var r5 = numeros.SkipWhile(n => n < 8);
            //Mostramos los resultados
            foreach (int n in r5)
                Console.WriteLine(n);
            Console.WriteLine("---------------");

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Proyeccion
            //Select transforma el elemento de entrada con la expresion lambda
            //selectMany transforma el elemento lo aplana y concatena con los subsecuencias resultantes

            //Hemos usado Select con tipos anonimos o para tomar el elemento completamente

            //Proyeccion indexada
            Console.WriteLine("---- Proyeccion indexada ----\r\n");
            IEnumerable<string> r6 = postres.Select((p, i) => "Indice " + i + " para el postre " + p);
            //Mostramos los resultados
            foreach (string postre in r6)
                Console.WriteLine(postre);
            Console.WriteLine("---------------");

            //SelectMany

            //En Select cada elemento de entrada produce un elemento de salida
            //SelectMany produce 0..n elementos.
            Console.WriteLine("---- SelectMany ----\r\n");
            IEnumerable<string> r7 = postres.SelectMany(p => p.Split());
            //Mostramos los resultados
            foreach (string postre in r7)
                Console.WriteLine(postre);
            Console.WriteLine("---------------");

            //Comparamos con Select
            Console.WriteLine("---- Comparamos con Select ----\r\n");
            //regresa un arreglo de cadenas no esta "aplanado2
            IEnumerable<string[]> r8 = postres.Select(p => p.Split());
            //Mostramos los resultados
            foreach (string[] n in r8)
            {
                Console.WriteLine("-");
                foreach (string m in n)
                    Console.WriteLine(m);
            }
            Console.WriteLine("---------------");

            //Select con multiples variables de rango
            Console.WriteLine("--- Select con multiples variables ---\r\n");
            IEnumerable<string> r9 = from p in postres
                                     from p1 in p.Split()
                                     select p1 + " ===> " + p;
            //Mostramos los resultados
            foreach (string postre in r9)
                Console.WriteLine(postre);
            Console.WriteLine("---------------");

            IEnumerable<string> r10 = from p in postres
                                     from p1 in postres
                                     select "Yo quiero: " + p1 + " y tu quieres: " + p;
            //Mostramos los resultados
            foreach (string postre in r10)
                Console.WriteLine(postre);
            Console.WriteLine("---------------");

            //union-Joining
            //Join une los elmentos de dos colecciones en un solo conjunto
            //GroupJoin es como Join pera da un resultado jerarquico
            //Zip Enumera dos secuencias y aplica una funcion para cada par

            Console.WriteLine("----- Join ------\r\n");
            List<CEstudiante> estudiantes = new List<CEstudiante>
            {
                new CEstudiante("Ana",100),
                new CEstudiante("Mario", 150),
                new CEstudiante("Susana", 180)
            };


            List<CCurso> cursos = new List<CCurso>
            {
                new CCurso("Programacion",100),
                new CCurso("Orientado objetos", 150),
                new CCurso("Programacion", 150),
                new CCurso("Programacion", 180),
                new CCurso("UML", 100),
                new CCurso("Orientado objetos", 100),
                new CCurso("UML", 180)
            };
            
            //Mismo resultado que el join
            //var listado = from e in estudiantes
            //              from c in cursos
            //              where c.Id == e.Id
            //              select e.Nombre + " esta en el curso " + c.Curso;
            ////Mostramos los resultados
            //foreach (string n in listado)
            //    Console.WriteLine(n);
            //Console.WriteLine("---------------");


            //No se puede utilizar == en su lugar equals
            //No funciona si se invierte a: c.Id equals e.Id
            var listado = from e in estudiantes
                          join c in cursos on e.Id equals c.Id
                          select e.Nombre + " esta en el curso " + c.Curso;
            //Mostramos los resultados
            foreach (string n in listado)
                Console.WriteLine(n);
            Console.WriteLine("---------------");

            Console.WriteLine("----- GroupJoin -----\r\n");
            //Los resultados se obtienen en forma jerarquica
            //la sintaxios es la misma pero utilizamos into
            var listado2 = from e in estudiantes
                          join c in cursos on e.Id equals c.Id
                          into tempListado
                          select new { estudiante = e.Nombre, id = e.Id, tempListado };
            //Mostramos los resultados
            foreach (var lst in listado2)
            {
                Console.WriteLine(lst.estudiante + "=> id:" + lst.id);
                foreach (var lst2 in lst.tempListado)
                    Console.WriteLine(lst2);
            }
                
            Console.WriteLine("---------------");

            //ZIP
            //regresa una secuencia que aplica una funcion a cada par
            //si llega a faltar un elemento con su par, no se muestra el par completo, eso se puede probar al borrar el elemento "ate"
            Console.WriteLine("----- ZIP -----\r\n");
            string[] helados = { "chocolate", "vainilla", "fresa", "limon","ate" };

            IEnumerable<string> r12 = postres.Zip(helados, (p, h) => p + " con helado de " + h);

            //Mostramos los resultados
            foreach (string n in r12)
                Console.WriteLine(n);
            Console.WriteLine("---------------");

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Ordenamiento
            //OrderBy, ThenBy ordena en orden ascendente
            //OrderByDescending, ThenByDescending Ordena en orden descendente
            //reverse regresa en el orden inverso

            Console.WriteLine("-------  Ordenamiento ---------\n\r");
            IEnumerable<int> numOrder = numeros.OrderBy(n=> n);
            IEnumerable<int> numDes = numeros.OrderByDescending(n => n);

            //Mostramos los resultados
            foreach (int n in numOrder)
                Console.WriteLine(n);
            Console.WriteLine("---------------");
            //Mostramos los resultados
            foreach (int n in numDes)
                Console.WriteLine(n);
            Console.WriteLine("---------------");

            string[] palabras = { "hola", "piedra", "pato", "pastel", "carros", "auto" };
            IEnumerable<string> palabrasOrd = palabras.OrderBy(p => p.Length).ThenBy(p=> p);
            //Mostramos los resultados
            foreach (string n in palabrasOrd)
                Console.WriteLine(n);
            Console.WriteLine("---------------");

            //////////////////////////////////////////////////////////////////////////////////////
            //Agrupamiento
            //GroupBy agrupa una secuencia en subsecuencias

            Console.WriteLine("---- Agrupamiento ----\r\n");
            string[] archivos = System.IO.Directory.GetFiles(@"C:\AMD\WU-CCC2\ccc2_install");
            Console.WriteLine("Archivos Obtenidos por GetFiles");
            foreach (string n in archivos)
                Console.WriteLine(n);

            //agrupamos basados en la extension
            //adentro de () colocamos el criterio de agrupamiento
            var archivoAg = archivos.GroupBy(a => System.IO.Path.GetExtension(a));

            Console.WriteLine("Resultados agrupados");
            foreach(IGrouping<string,string> g in archivoAg)
            {
                Console.WriteLine("Archivos de extension {0}", g.Key);      //aqui usamos la llave de agrupamiento
                foreach (string a in g)
                    Console.WriteLine("\t {0}", a);
            }

            ///////////////////////////////////////////////////////////////////////////////////
            //Conjuntos
            //Concat, Union, Intersect, except
            ///////////////////////////////////////////////////////////////////////////////////

            //Conversion
            //OfType        conviente de Inumerable a IEnumerable<T>, desecha los elementos erroneos
            //Cast          conviente de Inumerable a IEnumerable<T>, lanza excepcion con los elementos erroneos
            //ToArray       Conviente de IEnumerable<T> a T[]
            //ToList        convierte de IEnumerable<T> a List<T>
            //ToDictionary  convierte de IEnumerable<T> a Dictionary<TKey, TValue>
            //ToLookip      convierte de IEnumerable<T> a ILookup<Tkey, TElement>
            //AsEnumerable  hace downcast a IEnumerable<T>
            //AsQueryable   hace cast o convierte a IQueryable<T>

            ///////////////////////////////////////////////////////////////////////////////////

            //De elemento
            //First, FirsOrDefault      Regresa primer elemento de la secuencia
            //Last, LastOrDefault       regresa el ultimo elemento de la secuencia
            //Single, SingleOrDefault   equivalente a First, FirsOrDefault pera lanza una excepcion si hay mas de uno
            //ElementAt, ElementAtOrDefault     Regresa el elmento de determinada posicion
            //DefaultIfEmpty            Regresa al elemento de default si la secuencia no tiene elementos

            Console.WriteLine("--- De elemento ---\r\n");

            //Obtenemos el primero
            int primero = numeros.First();
            Console.WriteLine(primero);

            //Primero que cumpla con cierta condicion
            int primeroc = numeros.First(n => n % 2 == 0);
            Console.WriteLine(primeroc);

            //Primero que cumpla o default
            int primerod = numeros.FirstOrDefault(n => n % 57 == 0);
            Console.WriteLine(primerod);

            //////////////////////////////////////////////////////////////////////////////
            //agregacion
            //Count, LongCount  Regresa la cantidad de elementos en la secuencia en formato int o int64
            //Min               regresa el elemento menor de la secuencia
            //Max               regresa el elemento mayor de la secuencia
            //Sum               regresa la sumatoria de los elementos
            //Average           regresa el primedio de los elementos
            //Aggregate         hace una agregacion usando nuestro algoritmo

            Console.WriteLine("--- De agregación ---\r\n");

            int sumatoria = numeros.Sum();
            Console.WriteLine(sumatoria);

            int[] numeros2 = { 1, 2, 3, 4, 5 };
            int[] numeros3 = { 1, 5, 7, 3, 5, 9, 8, 11, 2, 4 };
            //En este caso la semilla es cero, si no se pone la semilla, toma el primer valor (t = variable de acumulacion, n = variable de ese momento)
            int agregado = numeros2.Aggregate(0, (t, n) => t + (n * 2));
            Console.WriteLine(agregado);

            ////////////////////////////////////////////////////////////////////////////
            //cuantificadores

            //Contains          Regresa true si la secuencia contiene al elemento
            //Any               regresa true si un elemento satisface al predicado
            //All               regresa true si todos los elementos satisfacen al predicado
            //sequenceEqual     regresa true si la segunda secuencia tiene elementos identicos a la entrada

            Console.WriteLine("--- Cuantificadores ---\r\n");
            bool todos = numeros2.All(n => n < 10);
            Console.WriteLine(todos);

            //resultado true
            bool iguales = numeros.SequenceEqual(numeros3);
            Console.WriteLine(iguales);
            //resultado true
            iguales = numeros.SequenceEqual(numeros2);
            Console.WriteLine(iguales);

            ///////////////////////////////////////////////////////////////////////////
            //Generacion

            //Empty     Crea una secuencia vacia
            //repeat    Crea una secuencia de elementos que se repiten
            //Range     Crea una secuencia de enteros

            var vacia = Enumerable.Empty<int>();
            foreach (int n in vacia)
                Console.WriteLine(n);
            Console.WriteLine("-----------");

            var repetir = Enumerable.Repeat("Hola",5);
            foreach (string n in repetir)
                Console.WriteLine(n);
            Console.WriteLine("-----------");

            var rango = Enumerable.Range(5,15);
            foreach (int n in rango)
                Console.WriteLine(n);
            Console.WriteLine("-----------");

            
            Console.Write("Presione una tecla para continuar. . . ");
            Console.ReadKey();
        }
    }
}
