﻿namespace LINQOperadores13
{
    internal class CEstudiante
    {
       
        public CEstudiante(string pNombre, int pID)
        {
            Nombre = pNombre;
            Id = pID;

        }

        public string Nombre { get; set; }
        public int Id { get; set; }

        public override string ToString()
        {
            return string.Format("Estudiante {0}, {1}",Nombre,Id);
        }


    }
}