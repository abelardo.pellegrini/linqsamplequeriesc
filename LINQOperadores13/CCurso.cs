﻿namespace LINQOperadores13
{
    internal class CCurso
    {
        
        public CCurso(string pCurso, int pID)
        {
            this.Curso = pCurso;
            this.Id = pID;
        }

        public string Curso { get ; set; }
        public int Id { get; set; }

        public override string ToString()
        {
            return string.Format("Curso =>{0}",Curso);
        }
    }
}