﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQClases04
{
    class Program
    {
        static void Main(string[] args)
        {
            //Uso de LINQ con clase

            //Creamos una lista
            List<CEstudiante> estudiantes = new List<CEstudiante>
            {
                new CEstudiante("Ana","A100","Mercadotecnia",10.0),
                new CEstudiante("Luis","S250","Orientado a objetos",9.0),
                new CEstudiante("Juan","S875","Programacion basica",5.0),
                new CEstudiante("Susana","A432","Mercadotecnia",8.7),
                new CEstudiante("Pablo","A156","Mercadotecnia",4.3),
                new CEstudiante("Alberto","S456","Orientado a objetos",8.3),
            };

            //encontramos a los reprobados
            var reprobados = from e in estudiantes
                             where e.Promedio <= 5.0
                             select e;

            //Mostramos
            Console.WriteLine("Reprobados");
            foreach (CEstudiante r in reprobados)
                Console.WriteLine(r);


            //Mostramos solo un atributo de los encontrados por medio de la propiedad
            Console.WriteLine("Solo un atributo");
            foreach (CEstudiante r in reprobados)
                Console.WriteLine(r.Nombre);

            //Encontramos solo los nombres de los de mercadotecnia
            var mercadotecnia = from m in estudiantes
                                where m.Curso == "Mercadotecnia"
                                orderby m.Nombre
                                select m.Nombre;

            //Mostramos solo el nombre de los alumnos que tienen mercadotecnia
            Console.WriteLine("Alumnos de Mercadotecnia");
            foreach (string m in mercadotecnia)
                Console.WriteLine(m);


            Console.Write("Presione una tecla para continuar...");
            Console.ReadKey(true);
        }
    }
}
