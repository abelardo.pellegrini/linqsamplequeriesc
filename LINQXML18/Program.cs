﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LINQXML18
{
    class Program
    {
        static void Main(string[] args)
        {
            var escuela = new XElement("Escuela",
                new XElement("Ciencias",
                    new XElement("Materia", "Matematicas"),
                    new XElement("Materia", "Fisica")),
                new XElement("Artes",
                    new XElement("Materia", "Historia del arte"),
                    new XElement("Practica", "Pintura"))
                );//Fin escuela

            Console.WriteLine(escuela);
            Console.WriteLine("-------------------------------");

            //Nodes regresa a los hijos
            foreach (XNode nodo in escuela.Nodes())
                Console.WriteLine(nodo.ToString());

            Console.WriteLine("-------------------------------");

            //Elements regresa los hijos de los nodos de tipo XElement
            foreach (XElement elemento in escuela.Elements())
                Console.WriteLine(elemento);    //.Name + "=" + elemento.Value);
            Console.WriteLine("-------------------------------");
            //Elements regresa los hijos de los nodos de tipo XElement otro tipo de resultado
            //Observar que elemento.Value devuelve todos los valores del elemento pero concatenados
            foreach (XElement elemento in escuela.Elements())
                Console.WriteLine(elemento.Name + "=" + elemento.Value);
            Console.WriteLine("-------------------------------");

            //FirstNode nos regresa el primer nodo
            Console.WriteLine("---------primer nodo--------------");
            Console.WriteLine(escuela.FirstNode);

            //Encontramos el padre del primer nodo
            Console.WriteLine("---------Padre del primer nodo------------");
            Console.WriteLine(escuela.FirstNode.Parent.Name);

            //LastNode nos regresa el ultimo nodo
            Console.WriteLine("--------ultimo nodo-----------");
            Console.WriteLine(escuela.LastNode);

            //Encontramos el padre del ultimo nodo
            Console.WriteLine("---------Padre del ultimo nodo------------");
            Console.WriteLine(escuela.LastNode.Parent.Name);

            //Obtiene todos los elementos del curso donde se encuentre fisica
            Console.WriteLine("---------elementos del curso que tienen fisica------------");
            IEnumerable<string> materias = from curso in escuela.Elements()
                                           where curso.Elements().Any(materia => materia.Value == "Fisica")
                                           select curso.Value;
            foreach (string s in materias)
                Console.WriteLine(s);

            //Obtiene el nombre del elemento padre de fisica
            Console.WriteLine("---------elemento padre de fisica------------");
            IEnumerable<XName> tipoCurso = from curso in escuela.Elements()
                                           where curso.Elements().Any(materia => materia.Value == "Fisica")
                                           select curso.Name;
            foreach (XName s in tipoCurso)
                Console.WriteLine(s);

            //Solamente selecciona los nodos materia
            Console.WriteLine("---------Selecciona nodos materia------------");
            IEnumerable<string> materia2 = from curso in escuela.Elements()
                                           from asignatura in curso.Elements()
                                           where asignatura.Name == "Materia"
                                           select asignatura.Value;
            foreach (string s2 in materia2)
                Console.WriteLine(s2);

            //Contamos los elementos de un tipo en particular
            Console.WriteLine("---------Contamos los elementos de un tipo en particular------------");
            int n = escuela.Elements("Ciencias").Count();
            Console.WriteLine("Hay {0} ciencias", n);

            //Element nos da la primera ocurrencia de un elemento con ese nombre
            Console.WriteLine("------Element nos da la primera ocurrencia de un elemento con ese nombre------");
            string mat = escuela.Element("Ciencias").Element("Materia").Value;
            Console.WriteLine(mat);

            //Obtenemos el siguiente nodo, estilo lista ligada
            Console.WriteLine("");
            Console.WriteLine("--- Obtenemos el siguiente nodo, estilo lista ligada ---");
            XNode s3 = escuela.FirstNode;
            Console.WriteLine(s3);
            Console.WriteLine("--- Tomamos el siguiente ---");
            s3 = s3.NextNode;
            Console.WriteLine(s3);

            //Otra forma de crear un elemento, el parent es escuela
            Console.WriteLine("");
            Console.WriteLine("--- crear un elemento, el parent es escuela ---");
            escuela.SetElementValue("Deportes","No hay");
            Console.WriteLine(escuela);
            Console.WriteLine("--------------------------------------");
            //Si lo volvemos a usar, se actualiza
            escuela.SetElementValue("Deportes", "Sin presupuesto");
            Console.WriteLine(escuela);

            //Para adicionar despues de un elemento, el primer nodo es el punto de referencia
            Console.WriteLine("");
            Console.WriteLine("--- adicionar despues de un elemento ---");
            escuela.FirstNode.AddAfterSelf(new XElement("AsignaturasDespues"));
            Console.WriteLine(escuela);

            //Para adicionar antes de un elemento, el primer nodo es el punto de referencia
            Console.WriteLine("");
            Console.WriteLine("--- adicionar antes de un elemento ---");
            escuela.FirstNode.AddBeforeSelf(new XElement("AsignaturasAntes"));
            Console.WriteLine(escuela);

            //Obtenemos a matematicas
            Console.WriteLine("");
            Console.WriteLine("--- cambiar valor de un elemento ---");
            var mate = escuela.Element("Ciencias").Element("Materia");
            //le damos un valor
            mate.SetValue("Geometria no Euclidiana");
            Console.WriteLine(escuela);

            //Obtenemos el valor de ese elemento
            string valorMate = mate.Value;
            Console.WriteLine(valorMate);

            //////////////////////////////////////////////////////////////////////////////////////
            //Crear el archivo que se utilizará en los ejemplos siguientes
            //XDocument alumnos = new XDocument(
            //    new XElement("Personas",
            //        new XElement("Alumnos",
            //            new XElement("Alumno", new XAttribute("Nombre","Jose"),
            //                new XElement("Curso","Programacion"), 
            //                new XElement("Calificacion",8)
            //                ),
            //            new XElement("Alumno", new XAttribute("Nombre", "Susana"),
            //                new XElement("Curso", "UML"),
            //                new XElement("Calificacion", 9)
            //                ),
            //            new XElement("Alumno", new XAttribute("Nombre", "Maria"),
            //                new XElement("Curso", "Orientado a objetos"),
            //                new XElement("Calificacion", 10)
            //                ),
            //            new XElement("Alumno", new XAttribute("Nombre", "Luis"),
            //                new XElement("Curso", "UML"),
            //                new XElement("Calificacion", 10)
            //                )
            //        ),
            //        new XElement("Maestros",
            //            new XElement("Maestro", new XAttribute("Nombre", "Juan"),
            //                new XElement("Curso", "Programacion"),
            //                new XElement("Horario", "Completo")
            //                ),
            //            new XElement("Maestro", new XAttribute("Nombre", "Denis"),
            //                new XElement("Curso", "UML"),
            //                new XElement("Horario", "Medio")
            //                ),
            //            new XElement("Maestro", new XAttribute("Nombre", "Sofia"),
            //                new XElement("Curso", "Orientado a objetos"),
            //                new XElement("Horario", "Completo")
            //                ),
            //            new XElement("Maestro", new XAttribute("Nombre", "Carlos"),
            //                new XElement("Curso", "Bases de datos"),
            //                new XElement("Horario", "Medio")
            //                )
            //        )
            //    )
            //);
            //alumnos.Save("alumnos.xml");

            //Descendants regresa a los nodos hijos y todos sus descendientes
            //Cargamos un XML de archivo
            //No olvidar poner un .xml en el directorio para cargar

            XDocument alumnos = XDocument.Load("alumnos.xml");
            //Lo mostramos
            Console.WriteLine(alumnos);
            Console.WriteLine("------------------------------------------");

            ////Eliminamos a los maestros
            //Console.WriteLine("");
            //Console.WriteLine("--- Eliminamos a los maestros ---");
            //alumnos.Descendants("Maestros").Remove();
            //Console.WriteLine(alumnos);
            //Console.WriteLine("------------------------------------------");

            ////Eliminamos calificaciones
            //Console.WriteLine("");
            //Console.WriteLine("--- Eliminamos las calificaciones ---");
            //alumnos.Descendants("Calificacion").Remove();
            //Console.WriteLine(alumnos);
            //Console.WriteLine("------------------------------------------");

            //Obtenemos las calificaciones
            var califs = from a in alumnos.Descendants("Calificacion")
                         select a.Value;
            foreach (var calif in califs)
                Console.WriteLine(calif);

            //////////////////////////////////
            Console.Write("Presione una tecla para continuar...");
            Console.ReadKey();

        }
    }
}
