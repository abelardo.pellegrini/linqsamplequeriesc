﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LINQXML16
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creamos un XML a partir de un arreglo
            //Funciona con otros contenedores tambien

            //Usamos tipos anonimos pero funciona con class normales tambien

            var listado = new[]
            {
                new {Nombre ="Jose", Calif=8, Curso="Programacion"},
                new {Nombre ="Susana", Calif=9, Curso="UML"},
                new {Nombre ="Maria", Calif=10, Curso="Orientado a objetos"},
                new {Nombre ="Luis", Calif=10, Curso="UML"},
            };

            //Ahora creamos el elemento
            XElement alumnos = new XElement("Alumnos",  //este es la raiz
                from a in listado
                select new XElement("Alumno", new XAttribute("Nombre",a.Nombre),
                    new XElement("Curso",a.Curso),
                    new XElement("Calificacion", a.Calif)
                    )//Fin de alumno
                    );//fin de alumnos

            //Lo mostramos
            Console.WriteLine(alumnos);

            //Lo mandamos a disco
            alumnos.Save("Alumnos.xml");


            Console.Write("Presione una tecla para continuar...");
            Console.ReadKey();
        }
    }
}
