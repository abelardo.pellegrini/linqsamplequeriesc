﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQMetodoClase03
{
    class Program
    {
        static void Main(string[] args)
        {
            //Obtener resultados de query desde metodos

            //Invocamos el metodo
            IEnumerable<int> resultados = CClaseExplicita.ObtenerNumerosPares();

            //Mostramos los resultados
            foreach (int num in resultados)
                Console.WriteLine(num);

            Console.WriteLine("---------");

            IEnumerable<string> postres = CClaseExplicita.ObtenerPostres();

            //Mostramos los resultados
            foreach (string p in postres)
                Console.WriteLine(p);

            Console.WriteLine("---------");
            int[] impares = CClaseExplicita.ObtenerNumerosImpares();

            //Mostramos los resultados
            foreach (int i in impares)
                Console.WriteLine(i);

            Console.WriteLine("Presione una tecla para continuar");
            Console.ReadKey(true);
        }
    }
}
