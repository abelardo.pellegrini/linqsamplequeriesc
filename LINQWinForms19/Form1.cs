﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace LINQWinForms19
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
        }
        XDocument doc = new XDocument();
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnCargar_Click(object sender, EventArgs e)
        {
            doc = XDocument.Load("alumnos.xml");
            //Mostramos en el textbox
            txtXML.Text = doc.ToString();
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            //Opcion personal
            //doc.Element("Alumnos").Add(new XElement("Alumno", new XAttribute("Nombre", txtNombre.Text), new XElement("Curso", txtCurso.Text), new XElement("Calificacion", txtCalificacion.Text)));

            //Opcion del tutorial
            //Creamos un nuevo elemento
            XElement temp = new XElement("Alumno", new XAttribute("Nombre", txtNombre.Text), 
                new XElement("Curso", txtCurso.Text), 
                new XElement("Calificacion", txtCalificacion.Text)
            );
            //Lo adicionamos al documento (aqui pareciera que lo inserta al inicio pero no es asi, lo inserta al final)
            doc.Descendants("Alumnos").First().Add(temp);

            //////////////////////////////////////////////////
            //actualizamos el textbox
            txtXML.Text = doc.ToString();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            //Opcion personal
            //var calif = from c in doc.Descendants("Alumno")
            //            where c.Element("Curso").Value == txtBusqueda.Text
            //            select c.Element("Calificacion").Value;
            //string result = "";
            //foreach (string c in calif)
            //    result = result + c + "\n\r";

            //MessageBox.Show(result);

            //Opcion del tutorial
            var resultados = from a in doc.Descendants("Alumno")
                             where (string)a.Element("Curso") == txtBusqueda.Text
                             select a.Element("Calificacion").Value + " " + a.Element("Curso").Value;
            //Construimos una cadena con la informacion
            string datos = "";
            foreach(var dato in resultados.Distinct())
            {
                datos += string.Format("Calificacion {0}\r\n", dato);
            }
            //Mostramos los resultados
            MessageBox.Show(datos);
             
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            //Creamos un documento de XML mas completo
            XNamespace xmlns = XNamespace.Get("http://www.sat.gob.mx/cfd/3");
            XNamespace xsi = XNamespace.Get("http://www.w3.org/2001/XMLSchema-instance");
            XNamespace schemaLocation = XNamespace.Get("http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/nomina12 http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina12.xsd");
            XNamespace schemaLocationNomina12 = XNamespace.Get("http://www.sat.gob.mx/nomina12 http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina12.xsd");
        
            XDocument documento = new XDocument(
                //new XDeclaration("1.0", "utf-8", "yes"),  //Colocamos la declaracion del documento
                //new XProcessingInstruction("xml-stylesheet", "href='MyStyles.css' title='Compact' type='text/css'"),

                new XElement("Comprobante"
                    ,new XAttribute("Certificado","")
                    ,new XAttribute("Descuento","")
                    , new XAttribute("Fecha", "")
                    , new XAttribute("Folio", "")
                    , new XAttribute("FormaPago", "")
                    , new XAttribute("LugarExpedicion", "")
                    , new XAttribute("MetodoPago", "PUE")
                    , new XAttribute("Moneda", "MXN")
                    , new XAttribute("NoCertificado", "")
                    , new XAttribute("Sello", "")
                    , new XAttribute("Serie", "")
                    , new XAttribute("SubTotal", "")
                    , new XAttribute("TipoDeComprobante", "")
                    , new XAttribute("Total", "")
                    , new XAttribute("Version", "")
                    , new XAttribute(XNamespace.Xmlns +"cfdi", "http://www.sat.gob.mx/cfd/3")
                    , new XAttribute(XNamespace.Xmlns + "nomina12", "http://www.sat.gob.mx/nomina12")
                    , new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance")
                    , new XAttribute(xsi + "schemaLocation", schemaLocation)
                    //, new XAttribute("", "")
                    //, new XAttribute("", "")
                    //, new XAttribute("", "")
                    
                    ,new XElement("Emisor"
                        , new XAttribute("Nombre", "")
                        , new XAttribute("RegimenFiscal", "603")
                        , new XAttribute("Rfc", "ICT9204249L8"))
                    , new XElement("Receptor"
                        , new XAttribute("Nombre", "")
                        , new XAttribute("UsoCFDI", "P01")
                        , new XAttribute("Rfc", "ICT9204249L8"))
                    , new XElement("Conceptos"
                        , new XElement("Concepto"
                            , new XAttribute("Cantidad", "1")
                            , new XAttribute("ClaveProdServ", "84111505")
                            , new XAttribute("ClaveUnidad", "ACT")
                            , new XAttribute("Descripcion", "Pago de nómina")
                            , new XAttribute("Descuento", "100.00")
                            , new XAttribute("Importe", "1000.00")
                            , new XAttribute("ValorUnitario", "1000.00"))
                        )//fin de Conceptos
                    , new XElement("Complemento"
                        , new XElement("Nomina"
                            , new XAttribute("FechaFinalPago", "2018-10-30")
                            , new XAttribute("FechaInicialPago", "2017-11-16")
                            , new XAttribute("FechaPago", "2017-11-30")
                            , new XAttribute("NumDiasPagados", "15")
                            , new XAttribute("TipoNomina", "O")
                            , new XAttribute("TotalDeducciones", "100.00")
                            , new XAttribute("TotalOtrosPagos", "0.00")
                            , new XAttribute("TotalPercepciones", "1000.00")
                            , new XAttribute("Version", "1.2")
                            , new XAttribute("TotalOtrosPagos", "0.00")
                            , new XAttribute("TotalOtrosPagos", "0.00")
                            , new XAttribute("TotalOtrosPagos", "0.00")
                            , new XAttribute("TotalOtrosPagos", "0.00")
                            , new XAttribute(XNamespace.Xmlns + "nomina12", "http://www.sat.gob.mx/nomina12")
                            , new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance")
                            , new XAttribute(xsi + "schemaLocation", schemaLocationNomina12))
                        )//fin de Complemento

                )//fin de Comprobante



            );//Fin del documento

            documento.Save("Prueba.xml");

            doc = XDocument.Load("Prueba.xml");
            //Mostramos en el textbox
            txtXML.Text = doc.ToString();
        }
    }
}
